var express = require("express");
const cv = require("opencv4nodejs");
const FPS = 30;

var app = express();

server = app.listen(5001, function () {
  console.log("server is running on port 5001");
});

require("./storeNewFaces")(cv, FPS);
