const fs = require("fs");

module.exports = (cv, FPS) => {
  const camera = new cv.VideoCapture(0);
  const classifier = new cv.CascadeClassifier(cv.HAAR_FRONTALFACE_ALT2);

  setInterval(() => {
    const frame = camera.read();
    const grayFrame = frame.cvtColor(cv.COLOR_BGR2GRAY);

    const photoFiles = fs.readdirSync("./photos");

    const { objects } = classifier.detectMultiScale(frame.bgrToGray());
    objects.forEach((rect, i) => {
      const faceRegion = grayFrame.getRegion(rect);
      let foundNewPerson = true;

      for (const file of photoFiles) {
        const referenceImg = cv.imread(`./photos/${file}`);
        const referenceGray = referenceImg.cvtColor(cv.COLOR_BGR2GRAY);

        const matched = faceRegion.matchTemplate(
          referenceGray,
          cv.TM_CCOEFF_NORMED
        );
        const minMax = matched.minMaxLoc();

        const threshold = 0.7;

        if (minMax.maxVal > threshold) {
          foundNewPerson = false;
          break;
        } else {
          continue;
        }
      }

      if (foundNewPerson) {
        cv.imwrite(`./photos/${Date.now()}.jpg`, faceRegion);
      }

      frame.drawRectangle(
        rect,
        new cv.Vec(foundNewPerson ? 0 : 255, 0, foundNewPerson ? 255 : 0),
        2,
        cv.LINE_8
      );
    });

    cv.imshow("Reconhecimento de imagem", frame);
    cv.waitKey(1);
  }, 1000 / FPS);
};
